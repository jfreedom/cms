import os
import argparse
from util import *

def main(indir):
	assert(os.path.isdir(indir))
	dirs = []
	files = []
	flt = FileFilter()
	for ff in os.listdir(indir):
		pathf = os.path.join(indir, ff)
		if os.path.isdir(pathf):
			dirs.append(ff)
		elif os.path.isfile(pathf) and flt.flt(ff):
			files.append(ff)
	print '## %s'%(indir)
	print '[../](../index.html)'
	for dd in dirs:
		if not flt.flt_dir(dd):
			print '[./%s](%s/index.html)'%(os.path.basename(dd), dd)
	xf = ExtTransform()
	for ff in files:
		ff = xf.xf(ff)
		name, ext = os.path.splitext(ff)
		if ext == '.pdf':
			print '<div><a href="%s" style="">%s<img style="vertical-align:middle" alt="%s" src="%s"></a></div>'%(
					os.path.basename(ff),name,name,name+'.png')
		else:
			print '[%s](%s)'%(name, os.path.basename(ff))
if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Compile markdown files to html')
	parser.add_argument('indir', type=str, help='Input directory to read')
	main(parser.parse_args().indir)
