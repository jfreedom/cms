OUTDIR = ./out
CMSDIR = ./cms
BASE_URL = file://$(abspath $(OUTDIR))

CMSABS = $(abspath $(CMSDIR))

TOPDIRS = $(shell find . -maxdepth 1 -and -type d -not -path $(CMSDIR) -not -path $(OUTDIR) -not -path ./.git -not -path ./out -print)
RECENT_FILES = $(patsubst %, $(OUTDIR)/%/recent.html, $(TOPDIRS))

DIRS = $(shell find . -type d -not -path $(CMSDIR)\* -not -path $(OUTDIR)\* -not -path ./.git\* -print)
INDEX_MD_TGT = $(patsubst %, $(OUTDIR)/%/index.md, $(DIRS))
INDEX_HTM_TGT = $(patsubst %, $(OUTDIR)/%/index.html, $(DIRS))

MD = $(shell find . -path $(OUTDIR) -prune -o -path $(CMSDIR) -prune -o -name '*.md' -print)
MD_TGT = $(patsubst %.md, $(OUTDIR)/%.html, $(MD))

PDF = $(shell find . -path $(OUTDIR) -prune -o -path $(CMSDIR) -prune -o -name '*.pdf' -print)
PDF_TGT = $(patsubst %.pdf, $(OUTDIR)/%.pdf, $(PDF))

TEX = $(shell find . -path $(OUTDIR) -prune -o -path $(CMSDIR) -prune -o -name '*.tex' -print)
TEX_TGT = $(patsubst %.tex, $(OUTDIR)/%.pdf, $(TEX))

IPYNB = $(shell find . -path $(OUTDIR) -prune -o -path $(CMSDIR) -prune -o -name '*.ipynb' -print)
IPYNB_MD_TGT = $(patsubst %.ipynb, $(OUTDIR)/%.md, $(IPYNB))
IPYNB_HTM_TGT = $(patsubst %.ipynb, $(OUTDIR)/%.html, $(IPYNB))

PDF_PREV = $(patsubst %.pdf, %.png, $(TEX_TGT)) $(patsubst %.pdf, %.png, $(PDF_TGT))


CSS = $(shell find $(CMSDIR)/css -name '*.css')
CSS_TGT = $(patsubst $(CMSDIR)/css/%.css,$(OUTDIR)/css/%.css, $(CSS))

all: $(CSS_TGT) $(OUTDIR)/index.html $(RECENT_FILES) $(TEX_TGT) $(PDF_TGT) $(IPYNB_HTM_TGT)  $(INDEX_HTM_TGT) $(MD_TGT) $(PDF_PREV) 

-include $(CMSDIR)/texdep.mk
-include $(CMSDIR)/pydep.mk

PY := $(CMSDIR)/venv/bin/python

$(shell date +"%Y-%m-%dT%H:%M:%S%z" >> activity_log)

$(shell mkdir -p $(OUTDIR))

$(CMSDIR)/.git_current: .FORCE
	@$(CMSDIR)/check_diff
.FORCE:

.INTERMEDIATE: $(OUTDIR)/index.md
$(OUTDIR)/index.md: $(CMSDIR)/.git_current $(CMSDIR)/home.py
	$(PY) $(CMSDIR)/home.py $(TOPDIRS) > $@

$(OUTDIR)/%/recent.md: $(CMSDIR)/.git_current
	mkdir -p $(@D)
	cd $*; $(CMSABS)/venv/bin/python $(CMSABS)/recent.py > $(abspath $@)

$(OUTDIR)/%.md: %.ipynb $(IPYTHON)
	mkdir -p $(@D)
	$(CMSDIR)/venv/bin/ipython nbconvert --to markdown $< 
	mv $(*F).md $@
	$(RM) -r $(@D)/$(*F)_files
	#Move $(*F)_files to $(OUTDIR) if it exists.
	find . -maxdepth 1 -name $(*F)_files -type d -print0 | xargs -0 -I{} mv {} $(@D) 

$(OUTDIR)/%.html: %.html
	mkdir -p $(@D)
	cp $< $@

$(OUTDIR)/css/pygments.css:
	mkdir -p $(@D)
	$(CMSDIR)/venv/bin/pygmentize -S default -f html >$@

%.html: %.md $(CMSDIR)/md.py $(CMSDIR)/md_template $(OUTDIR)/css/pygments.css
	$(CMSDIR)/venv/bin/python $(CMSDIR)/md.py --template $(CMSDIR)/md_template --base-url=$(BASE_URL) $< $@

$(OUTDIR)/%.pdf: %.tex $(PDFLATEX) $(IPYNB_HTM_TGT)
	mkdir -p $(@D)
	$(PDFLATEX) --halt-on-error --interaction=nonstopmode --output-directory=$(@D) $<
	$(RM) $(OUTDIR)/$*.aux
	$(RM) $(OUTDIR)/$*.log
	$(RM) $(OUTDIR)/$*.out

$(OUTDIR)/%.css: %.css
	mkdir -p $(@D)
	cp $< $@

$(OUTDIR)/%.pdf: %.pdf 
	mkdir -p $(@D)
	cp $< $@

$(OUTDIR)/%/index.md: $(CMSDIR)/index.py $(CMSDIR)/util.py %
	mkdir -p $(@D)
	$(PY) $(CMSDIR)/index.py $* > $@

%.png: %.pdf
	convert -thumbnail x300 -background white -alpha remove $^[0] $@
$(OUTDIR)/css/%.css: $(CMSDIR)/css/%.css
	mkdir -p $(@D)
	cp $< $@
fullclean:
	$(RM) -r $(OUTDIR)
clean: 
	$(RM) $(MD_TGT)
	$(RM) $(TEX_TGT)
	$(RM) $(IPYNB_HTM_TGT)

.PHONY: clean depclean  manclean
depclean: pydepclean texdepclean clean


