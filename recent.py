import argparse
import git
import StringIO
import time
import gitdb.exc
from util import *
import os
import sys
def gen_from_file_list(outstring, file_list, message, date):
	xf = ExtTransform()
	flt_changed_files =set() 
	for f in file_list:
		if not FileFilter().flt(f):
			flt_changed_files.add(f)
	if flt_changed_files:
		outstring.write('### %s - '%(message.split('\n')[0]))
		outstring.write('%s\n'%(time.strftime('%A %B %d, %Y - %H:%M', time.localtime(date))))
		for f in flt_changed_files:
			f = xf.xf(f)
			outstring.write('[%s](%s)\n'%(f, f))

		outstring.write('\n')

def gen_from_diff(outstring, diff, message, date):
	changed_files = []
	for x in diff:
		if x.a_blob is not None:
			changed_files.append(x.a_blob.path)
		if x.b_blob is not None:
			changed_files.append(x.b_blob.path)
	gen_from_file_list(outstring, changed_files, message, date)

def generate(gitrepo):
	assert gitrepo == '.'
	outstring = StringIO.StringIO()
	r = git.Repo(gitrepo)
	gen_from_file_list(outstring, r.untracked_files, 'UNTRACKED', time.time())
	gen_from_diff(outstring, r.index.diff(None), 'MODIFIED', time.time())
	try:
		gen_from_diff(outstring, r.index.diff('HEAD'), 'STAGED', time.time())
	except gitdb.exc.BadName:
		pass

	try:
		last_commit = None
		for c in r.iter_commits():
			if last_commit is not None:
				diff = c.diff(last_commit)
				message = last_commit.message
				date = c.authored_date
				gen_from_diff(outstring, diff, message, date)

			last_commit = c
	except ValueError:
		pass
	return outstring.getvalue()

if __name__ == '__main__':
	print generate('.')
