import os
class ExtTransform():
	def __init__(self):
		self.xfdict = {'.md':'.html','.ipynb':'.html','.tex':'.pdf'}
	def xf(self, fn):
		name, ext = os.path.splitext(fn)
		if self.xfdict.has_key(ext):
			ext = self.xfdict[ext]
		return '%s%s'%(name, ext)
class FileFilter():
	def __init__(self):
		self.whitelist = ['.html', '.pdf', '.tex', '.ipynb', '.md']
		self.blaclist_dir = ['git', 'cms', 'out']
		self.xf = ExtTransform()
	def flt_dir(self, fn):
		for dd in self.blaclist_dir:
			if fn.find(dd) >= 0:
				return True
		return False

	def flt(self, fn):
		'''Check if fn is in the whitelist for extions.'''
		zz1 = self._flt(fn)
		fnx = self.xf.xf(fn)
		zz2 = self._flt(fnx)
		zz3 = os.path.isfile(fn)
		rv = (zz1 or zz2) 
		return rv
	def _flt(self, fn):
		in_white_list = False
		for in_white_list_ext in self.whitelist:
			if fn.endswith(in_white_list_ext):
				in_white_list = True
				break
		return in_white_list


