IPYTHON = $(CMSDIR)/venv/bin/ipython

$(IPYTHON): $(CMSDIR)/requirements.txt
	virtualenv $(CMSDIR)/venv
	mkdir -p $(CMSDIR)/py-pkgs
	$(CMSDIR)/venv/bin/pip install --download=$(CMSDIR)/py-pkgs --requirement $(CMSDIR)/requirements.txt 
	$(CMSDIR)/venv/bin/pip install --find-links=$(CMSDIR)/py-pkgs --requirement $(CMSDIR)/requirements.txt 
	touch $(IPYTHON)
.PHONY: pydepclean 
pydepclean:
	$(RM) -r $(CMSDIR)/venv
