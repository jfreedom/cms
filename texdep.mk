OS = $(shell uname -s | tr '[:upper:]' '[:lower:]')
ARCH = $(shell uname -m)

ARCHOS = $(ARCH)-$(OS)
TLTAR = $(CMSDIR)/install-tl-unx.tar.gz

TEXBIN = $(CMSDIR)/tex/texlive/bin/$(ARCHOS)
export PATH := $(TEXBIN):$(PATH)
PDFLATEX = $(TEXBIN)/pdflatex

$(TLTAR):
	wget --directory-prefix=$(CMSDIR) http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz

TLINST = $(CMSDIR)/install-tl/install-tl

$(TLINST): $(TLTAR)
	tar -xf $^
	mv install-tl-* $(CMSDIR)/install-tl
	touch $(TLINST)

$(CMSDIR)/texlive.profile: $(CMSDIR)/texlive.profile.tmpl
	echo "# Auto generated file" > $@
	sed "s:<TEXDIR>:$(PWD)/$(CMSDIR)/tex:g" $^ >> $@

$(PDFLATEX): $(CMSDIR)/texlive.profile $(TLINST) $(CMSDIR)/texdep.mk
	$(TLINST) --profile $(CMSDIR)/texlive.profile
	#$(TEXBIN)/tlmgr install cancel titling mathtools bbm bbm-macros breqn l3kernel l3packages soul
	$(TEXBIN)/texconfig rehash
	$(TEXBIN)/fmtutil-sys --all
	$(TEXBIN)/texconfig rehash
	touch $(PDFLATEX)

.PHONY: texdepclean
texdepclean:
	$(RM) -r $(CMSDIR)/tex
	$(RM) -r $(CMSDIR)/install-tl
	$(RM) -r $(CMSDIR)/texlive.profile
