import argparse
import markdown
import os.path
import pystache
def convert(infile,
		    outfile, 
			template,
			base_url,
			extensions = ['markdown.extensions.codehilite',
				          'markdown.extensions.nl2br',
				          'markdown.extensions.sane_lists',
				          'markdown.extensions.tables',
						  'markdown.extensions.fenced_code',
				          'markdown.extensions.toc']):
	with open(infile, 'r') as infid:
		with open(outfile, 'w') as outfid:
			def condread(ss):
				if ss:
					if os.path.isfile(ss):
						with open(ss, 'r') as ssfid:
							ss = ssfid.read()
				return ss
			data = {'content':markdown.markdown(infid.read(), extensions), 'base_url':base_url}
			outfid.write(pystache.render(condread(template), data))



if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Compile markdown files to html')
	parser.add_argument('infile', type=str, help='Input file to convert')
	parser.add_argument('outfile', type=str, help='destination')
	parser.add_argument('--template', type=str, default='', help='Mustach template file')
	parser.add_argument('--base-url', type=str, default='./', help='Base URL site will be served from')
	args = parser.parse_args()
	convert(args.infile, args.outfile,args.template, args.base_url)
